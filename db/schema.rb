# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_12_021459) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "customers", force: :cascade do |t|
    t.string "email"
    t.boolean "accepts_marketing"
    t.string "first_name", limit: 20, null: false
    t.string "last_name", limit: 20, null: false
    t.integer "orders_count", default: 0
    t.integer "state"
    t.string "total_spent"
    t.bigint "last_order_id"
    t.text "note"
    t.boolean "verified_email"
    t.string "multipass_identifier"
    t.boolean "tax_exempt"
    t.string "phone"
    t.string "tags", default: ""
    t.string "last_order_name"
    t.string "currency"
    t.datetime "accepts_marketing_updated_at"
    t.string "marketing_opt_in_level"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "line_items", force: :cascade do |t|
    t.bigint "variant_id"
    t.bigint "product_id"
    t.string "title"
    t.string "sku"
    t.string "variant_title"
    t.string "vendor"
    t.string "fulfillment_service"
    t.string "name"
    t.string "variant_inventory_management"
    t.string "fulfillment_status"
    t.string "price"
    t.string "total_discount"
    t.boolean "requires_shipping", default: true
    t.boolean "taxable", default: true
    t.boolean "product_exists", default: true
    t.integer "quantity"
    t.integer "fulfillable_quantity"
    t.integer "grams"
    t.boolean "gift_card", default: false
    t.bigint "order_id"
    t.index ["order_id"], name: "index_line_items_on_order_id"
  end

  create_table "orders", force: :cascade do |t|
    t.string "email"
    t.datetime "closed_at"
    t.datetime "cancelled_at"
    t.integer "number"
    t.integer "total_weight"
    t.integer "order_number"
    t.text "note"
    t.string "token"
    t.string "currency"
    t.string "financial_status"
    t.string "cart_token"
    t.string "name"
    t.string "referring_site"
    t.string "landing_site"
    t.string "cancel_reason"
    t.string "subtotal_price"
    t.string "total_tax"
    t.string "total_discounts"
    t.string "total_line_items_price"
    t.boolean "taxes_included", default: false
    t.boolean "confirm", default: false
    t.boolean "buyer_accepts_marketing", default: false
    t.bigint "user_id"
    t.bigint "customer_id"
    t.bigint "shop_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["customer_id"], name: "index_orders_on_customer_id"
    t.index ["shop_id"], name: "index_orders_on_shop_id"
  end

  create_table "shops", force: :cascade do |t|
    t.string "shopify_domain", null: false
    t.string "shopify_token", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["shopify_domain"], name: "index_shops_on_shopify_domain", unique: true
  end

  add_foreign_key "line_items", "orders"
  add_foreign_key "orders", "customers"
  add_foreign_key "orders", "shops"
end
