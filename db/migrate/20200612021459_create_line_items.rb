class CreateLineItems < ActiveRecord::Migration[6.0]
  def change
    create_table :line_items, id: false do |t|
      t.bigint :id, null: false, primary_key: true
      t.bigint :variant_id
      t.bigint :product_id
      t.string :title
      t.string :sku
      t.string :variant_title
      t.string :vendor
      t.string :fulfillment_service
      t.string :name
      t.string :variant_inventory_management
      t.string :fulfillment_status
      t.string :price
      t.string :total_discount
      t.boolean :requires_shipping, default: true
      t.boolean :taxable, default: true
      t.boolean :product_exists, default: true
      t.integer :quantity
      t.integer :fulfillable_quantity
      t.integer :grams
      t.boolean :gift_card, default: false
      t.references :order, foreign_key: true, type: :bigint
    end
  end
end
