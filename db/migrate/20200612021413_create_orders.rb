class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders, id: false do |t|
      t.bigint :id, primary_key: true, null: false
      t.string :email
      t.timestamp :closed_at
      t.timestamp :cancelled_at
      t.integer :number
      t.integer :total_weight
      t.integer :order_number
      t.text :note
      t.string :token
      t.string :currency
      t.string :financial_status
      t.string :cart_token
      t.string :name
      t.string :referring_site
      t.string :landing_site
      t.string :cancel_reason
      t.string :subtotal_price
      t.string :total_tax
      t.string :total_discounts
      t.string :total_line_items_price
      t.boolean :taxes_included, default: false
      t.boolean :confirm, default: false
      t.boolean :buyer_accepts_marketing, default: false
      t.bigint :user_id
      t.references :customer, foreign_key: true, type: :bigint
      t.references :shop, foreign_key: true
      t.timestamps
    end
  end
end
