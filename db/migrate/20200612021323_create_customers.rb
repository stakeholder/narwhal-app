class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers, id: false do |t|
      t.bigint :id, null: false, primary_key: true
      t.string :email, unique: true
      t.boolean :accepts_marketing
      t.string :first_name, :last_name,  null: false, limit: 20
      t.integer :orders_count, default: 0
      t.integer :state
      t.string :total_spent
      t.bigint :last_order_id
      t.text :note
      t.boolean :verified_email
      t.string :multipass_identifier
      t.boolean :tax_exempt
      t.string :phone
      t.string :tags, default: ''
      t.string :last_order_name
      t.string :currency
      t.timestamp :accepts_marketing_updated_at
      t.string :marketing_opt_in_level
      t.timestamps
    end
  end
end
