ShopifyApp.configure do |config|
  config.application_name = 'NARWHAL ORDERS'
  config.api_key = ENV['SHOPIFY_API_KEY']
  config.secret = ENV['SHOPIFY_API_SECRET']
  config.old_secret = ''
  config.scope = 'write_products, write_orders' # Consult this page for more scope options:
  # https://help.shopify.com/en/api/getting-started/authentication/oauth/scopes
  config.embedded_app = true
  config.after_authenticate_job = false
  config.api_version = '2020-04'
  config.shop_session_repository = 'Shop'
  config.webhooks = [
    { topic: 'orders/create', address: "#{ENV['WEBHOOK_CALLBACK_URL']}/webhooks/orders_create", format: 'json' }
  ]
end

