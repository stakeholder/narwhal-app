class HomeController < AuthenticatedController
  before_action :fetch_shop
  def index
    @orders = Order.includes(:customer).where(shop: shop).limit(100) # fetches limited order
  end

  private

  attr_reader :shop
  def shop_param
    params.permit(:shop)['shop'] # extracts the shop_domain  from the request params
  end

  def fetch_shop
    @shop = Shop.find_by(shopify_domain: shop_param) # fetches shop using shop_domain
  end
end
