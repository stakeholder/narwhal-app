class Customer < ApplicationRecord
  has_many :orders, dependent: :delete_all

  def self.build(param)
    find_by(id: param['id']) || new(param.extract!(*column_names))
  end
end
