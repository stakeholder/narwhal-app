class Order < ApplicationRecord
  has_many :line_items, dependent: :destroy
  belongs_to :customer, optional: true
  belongs_to :shop

  def self.build(param)
    new(param.extract!(*column_names))
  end
end
