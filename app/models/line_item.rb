class LineItem < ApplicationRecord
  belongs_to :order

  def self.build(line_items_params)
    contains_narwhal_product = false
    instances = []
    line_items_params.each do |param|
      unless contains_narwhal_product
        contains_narwhal_product = true if param['title'].strip.downcase.split.include? 'narwhal'
      end
      instances << new(param.extract!(*column_names))
    end
    contains_narwhal_product ? instances : []
  end
end
