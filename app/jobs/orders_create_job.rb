class OrdersCreateJob < ActiveJob::Base
  def perform(shop_domain:, webhook:)
    line_items = LineItem.build webhook['line_items'] # builds a list of line_item instances
    return if line_items.empty? # stops jod if line_items array is empty

    order = Order.build webhook # builds an order instance
    order.line_items = line_items # assigns list_items instances to an order
    # builds and assign a customer instance to an order
    # only if customer data is present in the webhook hash
    order.customer = Customer.build webhook['customer'] if webhook['customer'].present?
    order.shop = Shop.find_by(shopify_domain: shop_domain) # assign shop domain to an order
    logger.error order.errors unless order.save
  rescue StandardError => e
    logger.error e
  end
end
