# [Narwhal Orders App](https://narwhal-orders-app.herokuapp.com/)

A simple private application for Shopify that list placed orders that contain  products with the word “narwhal” in them. The 
app monitors for new orders and checks to see if there are any products in the order that contain the word “Narwhal”. 
If there are, it adds a record in the DB.

## Development Tools
* [Ruby](https://www.ruby-lang.org/en/) - version  2.7.0
* [Rails](https://rubyonrails.org/) - version  6.0.3.1
* [ngrok](https://ngrok.com/)
* [PostgreSQL](https://www.postgresql.org/) 
* [Redis](https://redis.io/) 
* [Heroku](https://www.heroku.com/) - For hosting
* [Sidekiq](https://github.com/mperham/sidekiq) 

## Installation
1. Start up your terminal (or Command Prompt on Windows OS).
2. Ensure that you've `ruby`, `rails`, `redis`, `postgres`, and `ngrok` installed on your PC.
3. Clone the repository by entering the command `git@gitlab.com:stakeholder/narwhal-app.git` in the terminal.
4. Navigate to the project folder on your terminal and run the command `bundle install` to install required gems.
5. Run the command `bundle exec figaro install` to create `config/application.yml` file. This file holds the application environment values.
6. Add the following configuration into the file.

    ```
    DB_USERNAME: 'database username'
    DB_PASSWORD: 'database password'
    SHOPIFY_API_KEY: 'your shopify api key'
    SHOPIFY_API_SECRET: 'your shopify api secret'
    REDIS_URL: 'redis uri'
    WEBHOOK_CALLBACK_URL: 'webhook callback url' e.g https://706b5655629.ngrok.io
    ```
7. Run the command `rails db:migrate` to run migrations and create tables.

8. Run the following commands to start various servers
    * `rails server` to start the rails server
    * `redis-server` to start the redis server
    * `bundle exec sidekiq` to start sidekiq
    * `ngrok http { port on which the rails server is running }` to start ngrok
    

## Author
**Koya Gabriel.**

